import java.util.ArrayList;

public class Board {

    // Constants
    private static final int BOARDSIZE = 8;
    private static final int MOVES_TO_STALEMATE = BOARDSIZE * BOARDSIZE;
    private static final boolean WHITE = true, BLACK = false;

    // Attributes
    private Piece[][] board; // The pieces or free spaces of the board
    private int numMoves;    // The number of moves made

    // Constructor
    public Board() {

        // Set up the board array
        board = new Piece[BOARDSIZE][BOARDSIZE];

        // Put free spaces in the middle rows of the board
        for (int x = 0; x < BOARDSIZE; x++) {
            for (int y = 2; y < BOARDSIZE - 2; y++)
                board[x][y] = new Free();
        }

        // Place non-pawn pieces in correct spots
        board[0][0] = new Rook(BLACK, this);
        board[1][0] = new Knight(BLACK, this);
        board[2][0] = new Bishop(BLACK, this);
        board[3][0] = new Queen(BLACK, this);
        board[4][0] = new King(BLACK, this);
        board[5][0] = new Bishop(BLACK, this);
        board[6][0] = new Knight(BLACK, this);
        board[7][0] = new Rook(BLACK, this);

        board[0][7] = new Rook(WHITE, this);
        board[1][7] = new Knight(WHITE, this);
        board[2][7] = new Bishop(WHITE, this);
        board[3][7] = new Queen(WHITE, this);
        board[4][7] = new King(WHITE, this);
        board[5][7] = new Bishop(WHITE, this);
        board[6][7] = new Knight(WHITE, this);
        board[7][7] = new Rook(WHITE, this);

        // Place pawns
        for (int i = 0; i < BOARDSIZE; i++) {
            board[i][1] = new Pawn(BLACK, this);
            board[i][6] = new Pawn(WHITE, this);
        }

    }

    // Getters & Setters
    public Piece[][] getBoard() {
        return board;
    }
    public Piece getPieceAtPos(int col, int row) {
        return board[col][row];
    }
    private void setPieceAtPos(int col, int row, Piece piece) {
        board[col][row] = piece;
    }
    private void resetPos(int col, int row) {
        board[col][row] = new Free();
    }
    private void incNumMoves() {
        numMoves++;
    }

    // -----------------------------------------------------------------------------------------------------------------
    //        Method for printing the board
    // -----------------------------------------------------------------------------------------------------------------

    public void printBoard() {

        // Print letters at the top
        System.out.print(" ");
        for (int x = 0; x < BOARDSIZE; x++)
            System.out.print(" " + (char) (x + 'a'));
        System.out.println();

        for (int y = 0; y < BOARDSIZE; y++) {
            // Print numbers on the left side
            System.out.print(8 - y);

            // Print board fields
            for (int x = 0; x < BOARDSIZE; x++) {
                Piece piece = board[x][y];
                System.out.print(" " + piece.getSymbol());
            }

            // Print numbers on right side
            System.out.println(" " + (8 - y));
        }

        // Print letters at the bottom
        System.out.print(" ");
        for (int x = 0; x < BOARDSIZE; x++)
            System.out.print(" " + (char) (x + 'a'));
        System.out.println("\n");
    }

    // -----------------------------------------------------------------------------------------------------------------
    //        Methods for checking validity of moves and executing them
    // -----------------------------------------------------------------------------------------------------------------

    // Promote a pawn (used when it's already confirmed the piece is a pawn and is in position to be promoted)
    private void promotePawn(int col, int row) {
        // Make that spot a queen with the same color as the pawn was
        board[col][row] = new Queen(board[col][row].getColor(), this);
    }

    // Check if a move is valid
    public boolean isMoveValid(int[] move, boolean turn) {

        // Store the indices in variables for easy access
        int startCol = move[0];
        int startRow = move[1];
        int endCol = move[2];
        int endRow = move[3];

        // If indices are outside board size, move invalid
        for (int i : move) {
            if (i < 0 || i >= BOARDSIZE)
                return false;
        }

        // Store the piece at the start and end position in variables
        Piece pieceToMove = getPieceAtPos(startCol, startRow);
        Piece endPiece = getPieceAtPos(endCol, endRow);

        // If any of the following conditions is true, the move is invalid, so return false.

        return !(pieceToMove instanceof Free ||               // 1. There isn't a piece in the starting spot
                 pieceToMove.getColor() != turn ||            // 2. The person is moving the wrong color piece
                !pieceToMove.isValidPieceMove(move) ||        // 3. The move is invalid for that type of piece
                (startCol == endCol && startRow == endRow) || // 4. The move doesn't change the piece's position
                !pieceToMove.isPathClear(move) ||             // 5. The path isn't clear for the piece
                (!(endPiece instanceof Free) &&               // 6. The move would take a piece of the player's own color
                        endPiece.getColor() == pieceToMove.getColor()) ||
                willPutKingInCheck(move));                    // 7. The move will put the player's own king in check

    }

    // Moves a piece (used after having checked that the move is valid)
    public void movePiece(int[] move) {

        // Store the indices in variables for easy access
        int startCol = move[0];
        int startRow = move[1];
        int endCol = move[2];
        int endRow = move[3];

        // Store the piece to be moved in a variable
        Piece pieceToMove = getPieceAtPos(startCol, startRow);

        // Make the original location free
        resetPos(startCol, startRow);

        // Put the piece in the new spot
        setPieceAtPos(endCol, endRow, pieceToMove);

        // If it's a pawn:
        if (pieceToMove instanceof Pawn) {
            // If it's the pawn's first move, change the attribute so it no longer says it's the pawn's first move
            if (((Pawn) pieceToMove).isFirstMove())
                ((Pawn) pieceToMove).takeFirstMove();

                // Otherwise, if the pawn needs promoting (meaning it's hit the other side of the board), promote it
            else if ((endRow == 0 && pieceToMove.getColor()) ||
                     (endRow == (BOARDSIZE - 1) && !pieceToMove.getColor()))
                promotePawn(endCol, endRow);
        }

        // Increment number of moves made and change whose turn it is
       incNumMoves();
    }

    // -----------------------------------------------------------------------------------------------------------------
    //     Helper functions for Check and Checkmate
    // -----------------------------------------------------------------------------------------------------------------

    // Checks if there's any piece on the board which can take a piece in a spot
    private boolean canAnyPieceTake(int[] loc) {

        int col = loc[0], row = loc[1];
        Piece piece = board[col][row];

        // 1. Check for queens and rooks horizontally and vertically ---------------------------
        for (int i = 0; i < BOARDSIZE; i++) {

            Piece taker1 = board[i][row], taker2 = board[col][i];

            // If either piece isn't the original piece, it's a rook or queen,
            //    and is the right color, check if that can take the piece.
            if (taker1 != piece && (taker1 instanceof Rook || taker1 instanceof Queen) &&
                            (taker1.getColor() != piece.getColor())) {
                int[] move = {i, row, col, row};
                if (taker1.isPathClear(move))
                    return true;
            }

            if (taker2 != piece && (taker2 instanceof Rook || taker2 instanceof Queen) &&
                            (taker2.getColor() != piece.getColor())){
                int[] move = {col, i, col, row};
                if (taker2.isPathClear(move))
                    return true;
            }
        }

        // 2. Check for bishops and queens diagonally ------------------------------------------

        // Check in all directions
        for (int i = -1; i <= 1; i = i + 2) {
            for (int j = -1; j <= 1; j = j + 2) {

                int dist = 0, newCol = col, newRow = row;

                // Keep moving along the diagonal in the current direction until it's
                //    outside the board
                while  (newCol >= 0 && newCol < BOARDSIZE && newRow >= 0 && newRow < BOARDSIZE) {
                    Piece taker = board[newCol][newRow];
                    // If the piece is a bishop or queen, not the orig piece, and the right color,
                    //    it could work
                    if (taker != piece && (taker instanceof Bishop || taker instanceof Queen) &&
                        taker.getColor() != piece.getColor()) {
                        // Check if there's anything in the way; if no, then return true
                        int move[] = {newCol, newRow, col, row};
                        if (taker.isPathClear(move))
                            return true;
                    }

                    // Increment the distance and change newCol and newRow accordingly
                    dist++;
                    newCol = col + (i * dist);
                    newRow = row + (j * dist);
                }
            }
        }

        // 3. Check for knights that are an L shape away ---------------------------------------

        for (int colNum = 1; colNum <= 2; colNum++) {

            // rowNum = 2 if colNum = 1, rowNum = 1 if colNum = 2
            int rowNum = 3 - colNum;

            // Use those nums with every combo of 1 and -1
            for (int i = -1; i <= 1; i = i + 2) {
                for (int j = -1; j <= 1; j = j + 2) {

                    int newCol = col + (i * colNum), newRow = row + (j * rowNum);

                    // If no index problems, get the piece at that index
                    if (newCol >= 0 && newCol < BOARDSIZE && newRow >= 0 && newRow < BOARDSIZE) {
                        Piece knight = board[newCol][newRow];

                        // If it's a knight of the right color, it could take the piece
                        if (knight instanceof Knight && knight.getColor() != piece.getColor())
                            return true;
                    }
                }
            }
        }


        // 4. Check for pawns in position to attack --------------------------------------------

        // If the piece is black, look for white pawns on the next highest row;
        //    if white, look for black pawns on the next lowest row.
        int pawnDir;
        if (piece.getColor())
            pawnDir = -1;
        else
            pawnDir = 1;

        Piece pawn1 = new Piece(), pawn2 = new Piece();

        // Only look for pawns if the indices work
        if (row + pawnDir >= 0 && row + pawnDir < BOARDSIZE) {
            if (col + 1 < BOARDSIZE)
                pawn1 = board[col + 1][row + pawnDir];
            if (col >= 0)
                pawn2 = board[col - 1][row + pawnDir];
        }

        // If either piece is a pawn and is the opposite color to the piece, it could take the piece
        if (((pawn1 instanceof Pawn) && pawn1.getColor() != piece.getColor()) ||
                ((pawn2 instanceof Pawn) && pawn1.getColor() != piece.getColor()))
            return true;


        // 5. Check for kings one space away --------------------------------------------------
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                // Make the new col and row
                int newCol = col + i, newRow = row + j;
                // If the indices work, check that piece
                if (newCol >= 0 && newCol < BOARDSIZE && newRow >= 0 && newRow < BOARDSIZE) {
                    Piece king = board[newCol][newRow];
                    // If it's a king of the right color that's not the piece in question, true
                    if (king != piece && king instanceof King && king.getColor() != piece.getColor())
                        return true;
                }
            }
        }

        // If none of these conditions returned true, no piece can take the piece
        return false;
    }

    // Finds the king of a desired color on the board
    private int[] findKing(boolean color) {
        int[] loc = {-1, -1};
        // Loop through board to find the king
        for (int i = 0; i < BOARDSIZE; i++) {
            for (int j = 0; j < BOARDSIZE; j++) {
                Piece piece = board[i][j];
                // If the piece at a spot is a king and the right color, true
                if (piece instanceof King && piece.getColor() == color) {
                    loc[0] = i;
                    loc[1] = j;
                    return loc;
                }
            }
        }
        return loc;
    }

    // Finds the location of the king in check
    private int[] findKingInCheck() {
        if (whichKingInCheck())
            return findKing(WHITE);
        else
            return findKing(BLACK);
    }

    // Find out which king is currently in check, assuming we know one is in check
    public boolean whichKingInCheck() {
        return isKingInCheck(findKing(WHITE));
    }

    // Checks if a king is in check
    private boolean isKingInCheck(int[] loc) {

        int col = loc[0], row = loc[1];

        // Make sure col and row are in bounds
        if (col < 0 || col >= BOARDSIZE || row < 0 || row >= BOARDSIZE)
            return false;

        // If it's not a king, then can't be in check
        if (!(board[col][row] instanceof King))
            return false;

        // The king is in check if there is any piece on the board which is in position
        //    to take it
        return canAnyPieceTake(loc);
    }

    // Checks if the move will put the king in check
    private boolean willPutKingInCheck(int move[]) {

        // Store the indices in variables for easy access
        int startCol = move[0];
        int startRow = move[1];
        int endCol = move[2];
        int endRow = move[3];

        // Store pieces for easy access
        Piece pieceToMove = getPieceAtPos(startCol, startRow);
        Piece endPiece = getPieceAtPos(endCol, endRow);

        // Set the piece in the proposed position temporarily
        setPieceAtPos(endCol, endRow, pieceToMove);
        resetPos(startCol, startRow);

        // Find the king of the same color as the piece to be moved
        int[] kingLoc = findKing(pieceToMove.getColor());

        // Variable to store result: if it's in check, will return true
        boolean result = isKingInCheck(kingLoc);

        // Put pieces back to where they were and return the result
        setPieceAtPos(endCol, endRow, endPiece);
        setPieceAtPos(startCol, startRow, pieceToMove);
        return result;
    }

    // -----------------------------------------------------------------------------------------------------------------
    //     Check, Checkmate, and Stalemate
    // -----------------------------------------------------------------------------------------------------------------


    // Checks if the game is in check
    public boolean isInCheck() {
        // It's in check if the white or black king is in check
        return isKingInCheck(findKing(WHITE)) || isKingInCheck(findKing(BLACK));
    }

    // Checks if the game is in checkmate
    public boolean isInCheckmate(boolean turn) {

        // Can't be in checkmate if it isn't in check
        if (!isInCheck()) {
            return false;

        } else {
            // Store info about the king in check
            int[] kingLoc = findKingInCheck();
            int kingCol = kingLoc[0], kingRow = kingLoc[1];
            boolean kingColor = board[kingCol][kingRow].getColor();

            // Check all possible moves the person could make and see if it gets the king out of check
            for (int col = 0; col < BOARDSIZE; col++) {
                for (int row = 0; row < BOARDSIZE; row++) {

                    Piece currPiece = board[col][row];

                    // Only look into it if the piece is the player's color and isn't a free space
                    if (currPiece.getColor() == kingColor && !(currPiece instanceof Free)) {

                        // Make a list of the possible moves that piece could make
                        ArrayList<int[]> potentialMoves = new ArrayList<>();

                        // Possible moves depend on the type of piece:

                        if (currPiece instanceof Rook || currPiece instanceof Bishop || currPiece instanceof Queen) {

                            // If a rook or a queen, do this
                            if (!(currPiece instanceof Bishop)) {
                                // Rooks and queens can move in one direction only
                                for (int i = 0; i < BOARDSIZE; i++) {
                                    int[] move1 = {col, row, i, row};
                                    int[] move2 = {col, row, col, i};
                                    potentialMoves.add(move1);
                                    potentialMoves.add(move2);
                                }
                            }
                            // If a bishop or a queen, do this
                            if (!(currPiece instanceof Rook)) {
                                // Bishops and queens can move diagonally, so check all the way along each diagonal
                                for (int i = 0; i < BOARDSIZE; i++) {
                                    int[] move1 = {col, row, col + i, row + i};
                                    int[] move2 = {col, row, col - i, row + i};
                                    int[] move3 = {col, row, col + i, row - i};
                                    int[] move4 = {col, row, col - i, row - i};
                                    potentialMoves.add(move1);
                                    potentialMoves.add(move2);
                                    potentialMoves.add(move3);
                                    potentialMoves.add(move4);
                                }
                            }

                        } else if (currPiece instanceof King) {
                            // King can only move one spot in any direction
                            for (int i = -1; i < 2; i++) {
                                for (int j = -1; j < 2; j++) {
                                    int[] move = { col, row, col + i, row + j };
                                    potentialMoves.add(move);
                                }
                            }

                        } else if (currPiece instanceof Knight) {
                            // Knight can only move such that the row changes by 2 and col changes by 1, or vice versa
                            for (int i = 1; i <= 2; i++) {
                                int j = 3 - i;

                                int[] move1 = { col, row, col + i, row + j };
                                int[] move2 = { col, row, col - i, row + j };
                                int[] move3 = { col, row, col + i, row - j };
                                int[] move4 = { col, row, col - i, row - j };
                                potentialMoves.add(move1);
                                potentialMoves.add(move2);
                                potentialMoves.add(move3);
                                potentialMoves.add(move4);
                            }

                        } else if (currPiece instanceof Pawn) {
                            // Pawn can only move in a certain direction depending on color
                            int dir;
                            if (currPiece.getColor())
                                dir = -1;
                            else
                                dir = 1;

                            // Add all potential moves to the ArrayList
                            int[] move1 = { col, row, col, row + dir };
                            int[] move2 = { col, row, col, row + 2 * dir };
                            int[] move3 = { col, row, col + 1, row + dir };
                            int[] move4 = { col, row, col - 1, row + dir };
                            potentialMoves.add(move1);
                            potentialMoves.add(move2);
                            potentialMoves.add(move3);
                            potentialMoves.add(move4);
                        }

                        for (int[] move : potentialMoves) {
                            // If any of these moves is a valid move that will take the king out of check, then we're not
                            //    in checkmate
                            if (isMoveValid(move, turn) && !willPutKingInCheck(move)) {
                                return false;
                            }
                        }
                    }
                }
            }
            // If none of these returned a move that would take the king out of check, the game is in checkmate
            return true;
        }
    }

    // Checks if the game is a stalemate
    public boolean isStalemate() {
        return numMoves == MOVES_TO_STALEMATE;
    }

}
