public class Knight extends Piece {

    // Constants
    protected static final char WHITEKNIGHT  = '♘', BLACKKNIGHT  = '♞';

    // Constructor
    public Knight(boolean color, Board board) {
        super(color, board);

        if (color)
            symbol = WHITEKNIGHT;
        else
            symbol = BLACKKNIGHT;
    }

    // Methods
    @ Override
    public boolean isValidPieceMove(int[] move) {
        // Store abs values of differences b/w columns and rows
        int absColDiff = Math.abs(move[0] - move[2]);
        int absRowDiff = Math.abs(move[1] - move[3]);

        // The knight can only move in an "L" shape
        return  (absColDiff == 2 && absRowDiff == 1) ||
                (absColDiff == 1 && absRowDiff == 2);
    }

}

