public class Game {

    // Constants
    private static final char[] HORIZ_INDEX = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
    private static final boolean WHITE = true;
    private static final String INSTRUCTIONS = "- - - - - - - CHESS - - - - - - -\n" +
                                               "   Instructions: To play chess, \n" +
                                               " enter the location of the piece \n" +
                                               "  to be moved (ex. \"a4\"), then \n" +
                                               " the location you'd like to move \n" +
                                               "          the piece to.\n\n" +
                                               "    Enter y if ready to play,\n " +
                                               "           n to quit." ;

    private static final String WHITEPLAYS_MSG = "White plays. Enter move:", BLACKPLAYS_MSG = "Black plays. Enter move:";
    private static final String ILLEGALMOVE_MSG = "Illegal move!";
    private static final String WHITEWINS_MSG = "White wins!", BLACKWINS_MSG = "Black wins!";
    private static final String CHECK_MSG = "Check!", CHECKMATE_MSG = "Checkmate!", STALEMATE_MSG = "Stalemate!";

    // Attributes
    private Board board;
    private boolean turn;
    private boolean inProgress;

    // Constructor
    public Game() {
        board = new Board();
        turn = WHITE;
        inProgress = false;
    }

    // Getters & Setters
    public void changeTurn() {
        turn = !turn;
    }
    public void startGame() {
        inProgress = true;
    }
    public void endGame() {
        inProgress = false;
    }


    // -----------------------------------------------------------------------------------------------------------------
    //     Game-play Method
    // -----------------------------------------------------------------------------------------------------------------

    public void play() {

        // Make new reader and board objects
        EasyIn2 reader = new EasyIn2();
        board = new Board();

        // Print instructions and ask for input
        System.out.println(INSTRUCTIONS);
        char start = reader.getChar();

        // Start the game or quit based on user input
        if (start == 'y')
            startGame();
        else if (start == 'n')
            return;

        // Play the game
        while (inProgress) {

            // Declare variable to store validity of future moves
            boolean valid;

            // Do these things & repeat as needed until user inputs a valid move
            do {
                // Print board
                board.printBoard();

                // Print message depending on whose turn it is
                if (turn)
                    System.out.println(WHITEPLAYS_MSG);
                else
                    System.out.println(BLACKPLAYS_MSG);

                // Take user input and store
                String startPos = reader.getString();

                // If user inputs "quit", quit the game; otherwise, take the second string
                if ((startPos.toLowerCase()).equals("quit"))
                    return;
                String endPos = reader.getString();

                // Convert move to integer indices
                int[] move = getMoveIndices(startPos, endPos);

                // Check if move is valid
                valid = board.isMoveValid(move, turn);

                // If valid, move the piece
                if (valid) {
                    board.movePiece(move);
                    changeTurn();
                } else
                    System.out.println(ILLEGALMOVE_MSG);

            } while (!valid);

            // After a turn, check if the game is over
            if (board.isInCheckmate(turn) || board.isStalemate()) {
                board.printBoard();
                endGame();

                // Print out the correct message depending on why the game ended
                if (board.isInCheckmate(turn)) {
                    System.out.println(CHECKMATE_MSG);
                    if (board.whichKingInCheck())
                        System.out.println(BLACKWINS_MSG);
                    else
                        System.out.println(WHITEWINS_MSG);
                    // Otherwise, it was a stalemate, so print that message
                } else
                    System.out.println(STALEMATE_MSG);
            }
            // If a king is in check, print the check message
            else if (board.isInCheck())
                System.out.println(CHECK_MSG);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------
    //     Method to Convert String Input to Integer Values
    // -----------------------------------------------------------------------------------------------------------------

    // Take a user's string input and convert it to an array containing the integer indices
    public int[] getMoveIndices(String start, String end) {

        // Declare an array of integers that we will later return
        int[] move;

        // First make sure the inputs are strings of the right length.
        // If they're not, set the move to a simple array of -1s that won't be valid when checked later
        if (start.length() != 2 || end.length() != 2)
            move = new int[] { -1, -1, -1, -1 };

        // If they're the right length, go ahead and transform them into indices
        else {
            // Get the board rows from the user's input, set cols as -1 for now
            int startCol = -1, startRow = 8 - Character.getNumericValue(start.charAt(1));
            int endCol = -1, endRow = 8 - Character.getNumericValue(end.charAt(1));

            // For the columns, have to convert the chars to ints:
            // Loop through the horizontal index array to find the index of the
            //    given character and set the column index to that number
            // If that character isn't in the horizontal index array, it's not
            //    a valid index, so the column index will remain -1
            for (int i = 0; i < HORIZ_INDEX.length; i++) {
                if (HORIZ_INDEX[i] == start.charAt(0))
                    startCol = i;
            }
            for (int i = 0; i < HORIZ_INDEX.length; i++) {
                if (HORIZ_INDEX[i] == end.charAt(0))
                    endCol = i;
            }

            // Store these indices in an array
            move = new int[] { startCol, startRow, endCol, endRow };
        }
        // Return the move array
        return move;
    }



}
