public class Queen extends Piece {

    // Constants
    protected static final char WHITEQUEEN = '♕', BLACKQUEEN = '♛';

    // Constructor
    public Queen(boolean color, Board board) {
        super(color, board);

        if (color)
            symbol = WHITEQUEEN;
        else
            symbol = BLACKQUEEN;
    }

    // Methods
    @Override
    public boolean isValidPieceMove(int[] move) {

        // Store differences (and absolute values) between indices in variables
        int colDiff = move[0] - move[2];
        int rowDiff = move[1] - move[3];
        int absColDiff = Math.abs(colDiff);
        int absRowDiff = Math.abs(rowDiff);

        // The queen can move if it moves vertically, horizontally,
        //    or diagonally
        return  (colDiff == 0 && rowDiff != 0) ||
                (colDiff != 0 && rowDiff == 0) ||
                (absColDiff == absRowDiff);
    }

    @Override
    public boolean isPathClear(int[] move) {

        // Store indices in variables for easy access
        int startCol = move[0];
        int startRow = move[1];
        int endCol = move[2];
        int endRow = move[3];

        // Store differences (and abs values) between indices in variables
        int colDiff = startCol - endCol;
        int rowDiff = startRow - endRow;
        int absColDiff = Math.abs(colDiff);
        int absRowDiff = Math.abs(rowDiff);

        // Either it's moving diagonally, vertically, or horizontally.

        // Decrement col/row if col/row diff is positive,
        // increment if negative, and set to 0 if no difference
        int colDir = 0, rowDir = 0;

        if (colDiff != 0)
            colDir = (colDiff / absColDiff) * -1;

        if (rowDiff != 0)
            rowDir = (rowDiff / absRowDiff) * -1;

        // Check each place along the way to see if anything is in the path
        // NOTE: Using math.max works because either one of them is zero or they're the same.
        //    In neither case does setting i < that take the following declaration out of bounds of the board array.
        for (int i = 1; i < Math.max(absColDiff, absRowDiff); i++) {
            Piece spot = board.getBoard()[startCol + (i * colDir)][startRow + (i * rowDir)];
            if (!(spot instanceof Free))
                return false;
        }

        return true;
    }

}
