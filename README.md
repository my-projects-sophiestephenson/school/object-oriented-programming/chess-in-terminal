## University of St Andrews, CS1002, Week 9 Practical: Chess

### Assignment Details
The original assignment was to create a program that allowed two users to play a 
simplified game of chess in the terminal. This simplified version would include
only rooks and bishops, allowing someone to win the game by taking all of the
other player's pieces. I extended the assignment to allow the players to play a 
full game of chess including all 32 pieces. 

### How to Use
To play the game, import all files and run these two lines in the terminal:
```sh
javac *.java
java Chess
```
From there, you can start playing the game. Enter moves by first entering the
location of the piece you'd like to move - for example, "a4" - then the location
you'd like to move the piece to. 

The game is best played with the terminal background in white and the text in
black. This way, the pieces show up as the correct color. 