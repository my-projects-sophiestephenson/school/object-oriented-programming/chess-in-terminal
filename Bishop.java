public class Bishop extends Piece {

    // Constants
    private static final char WHITEBISHOP  = '♗', BLACKBISHOP  = '♝';

    // Constructor
    public Bishop(boolean color, Board board) {
        super(color, board);

        if (color)
            symbol = WHITEBISHOP;
        else
            symbol = BLACKBISHOP;
    }

    // Methods
    @ Override
    public boolean isValidPieceMove(int[] move) {
        // Store abs values of differences between columns and rows
        int absColDiff = Math.abs(move[0] - move[2]);
        int absRowDiff = Math.abs(move[1] - move[3]);

        // If the column and row didn't change the same amount, then
        //    the piece didn't move diagonally, so move invalid
        return (absColDiff == absRowDiff);
    }

    @Override
    public boolean isPathClear(int[] move) {

        // Store indices in variables for easy access
        int startCol = move[0];
        int startRow = move[1];
        int endCol = move[2];
        int endRow = move[3];

        // Store differences (and abs values) between indices in variables
        int colDiff = startCol - endCol;
        int rowDiff = startRow - endRow;
        int absColDiff = Math.abs(colDiff);
        int absRowDiff = Math.abs(rowDiff);

        // Decrement col/row if col/row diff is positive and increment if negative
        int colDir = (colDiff / absColDiff) * -1;
        int rowDir = (rowDiff / absRowDiff) * -1;

        // Check each place along the way to see if anything is in the path
        for (int i = 1; i < absColDiff; i++) {
            Piece spot = board.getBoard()[startCol + (i * colDir)][startRow + (i * rowDir)];
            if (!(spot instanceof Free))
                return false;
        }

        return true;
    }

}
